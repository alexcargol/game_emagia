<?php


namespace Characters;
use Characters\Skills;
use Characters\Type;

class Persona extends Base
{
    public $name;

    public $health;

    public $strength;

    public $defence;

    public $speed;

    public $luck;

    private $skills;

    public $type_id;


    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param mixed $health
     * @return Persona
     */
    public function setHealth($health)
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param mixed $strength
     * @return Persona
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @param mixed $defence
     * @return Persona
     */
    public function setDefence($defence)
    {
        $this->defence = $defence;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param mixed $speed
     * @return Persona
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @param mixed $luck
     * @return Persona
     */
    public function setLuck($luck)
    {
        $this->luck = $luck;
        return $this;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Persona
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Persona
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
       if (!empty(\Characters\Skills::findBy(['persona_id'=>$this->getId()]))){
        $allSkills = \Characters\Skills::findBy(['persona_id'=>$this->getId()]);
        return $allSkills; } else {return false;}
    }

    /**
     * @param mixed $skills
     * @return Persona
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type_id;
    }

    /**
     * @param mixed $type_id
     * @return Persona
     */
    public function setTypeId($type_id)
    {
        $this->type_id = $type_id;
        return $this;
    }

    public function isRapidStrike()
    {
        $allskills = $this->getSkills();
     if (!empty($allskills))
     { foreach ($allskills as $skill)
        { if (($skill->getName()=='Rapid Strike') && (($skill->getValue()=='yes'))) { return true; }
        }
     }
    }

    public function isMagicShield()
    {
        $allskills = $this->getSkills();
        if (!empty($allskills))
        {
        foreach ($allskills as $skill)
            { if (($skill->getName()=='Magic Shield') && (($skill->getValue()=='yes'))) { return true; }
            }
        }
    }

    public static function tableName(){
        return 'persona';
    }


}