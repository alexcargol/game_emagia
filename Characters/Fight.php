<?php


namespace Characters;
use Characters\Rounds;

class Fight extends Base
{

    public $id;

    public $winner_id;


    /**
     * @return mixed
     */
    public function getWinnerId()
    {
        return $this->winner_id;
    }

    /**
     * @param mixed $winner_id
     * @return Fight
     */
    public function setWinnerId($winner_id)
    {
        $this->winner_id = $winner_id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Fight
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


 static public function RapidStrike($attacker,$defender)
    {
        $aName = $attacker->getName();
        $dName = $defender->getName();
        $aStrength = $attacker->getStrength();
        $dDefence = $defender->getDefence();
        if ($attacker->isRapidStrike()==true) {
            $damage= $aStrength - $dDefence;
            $_SESSION["damage"]=$damage;
            $dHealth = ($defender->getHealth() - $damage);
            $defender->setHealth($dHealth);
            echo "<div>Cu un Rapid Strike $aName produce $damage damage </div>
           <div>$dName ramane cu health: $dHealth</div>";

            }



    }


  static public function checkWinner($attacker,$defender,$rounds)
    {
        $_SESSION["is_winner"] = false;
        $aName = $attacker->getName();
        $dName = $defender->getName();
        $aHealth = $attacker->getHealth();
        $dHealth = $defender->getHealth();

        if (($aHealth<0) || ($aHealth==0)) {

            $_SESSION["is_winner"] = true;
            return $defender->getId();
        }
        if (($dHealth<0) || ($dHealth==0)) {

            $_SESSION["is_winner"] = true;
            return $attacker->getId();     }

        if (($rounds==20) || ($rounds>20) ) {

            if ($aHealth>$dHealth) {

                $_SESSION["is_winner"] = true;
                return $attacker->getId();
                                }

            if ($aHealth<$dHealth) {

                $_SESSION["is_winner"] = true;
                return $defender->getId();
                                }

            if ($aHealth==$dHealth) {
                       return false;
                    }
        }

    }


   static public function attack($attacker, $defender)
    {
        $aName = $attacker->getName();
        $dName = $defender->getName();
        $aStrength = $attacker->getStrength();
        $dDefence = $defender->getDefence();
        $damage = $aStrength - $dDefence;
        echo "<br><div>Ataca $aName</div>";
        $MagicShield = $defender->isMagicShield();
        if ($MagicShield==true){ $damage = ($damage - $damage*($MagicShield->getEffect()/100));
            echo"<div>$dName are Magic Shield care il protejeaza si injumatateste damage-ul primit pana la $damage</div>";}
        $dHealth = $defender->getHealth()-$damage;
        $defender->setHealth($dHealth);
        echo "<div>Lovitura lui $aName produce $damage damage.</div>
            <div>$dName ramane cu $dHealth health.</div>";
        $_SESSION["damage"]=$damage;
        $_SESSION["player"]=$dName;

    }

    static public function startBattle($hero,$beast)
    {
        $Battle = new \Characters\Fight();
        $Battle->save();
        $battle_id = $Battle->getId();

        $heroName=$hero->getName();
        $beastName=$beast->getName();

        for($rounds=1; $rounds<21; $rounds++)
        {
            $ThisRound = new \Characters\Rounds();
            $ThisRound ->setGameId($battle_id);
            $ThisRound ->setRoundId($rounds);
            $ThisRound->save();
                echo"<br><div><b>Runda $rounds:</b></div>";

            if ($rounds == 1) {

                if ($hero->getSpeed() == $beast->getSpeed()) {
                    if ($hero->getLuck() > $beast->getLuck()) { $attacker = $hero; $defender = $beast;
                        $ThisRound->setAttackerId($attacker->getId());
                        $ThisRound->setDefenderId($defender->getId());
                        echo "<br>$heroName este mai norocos si loveste primul !";
                        static::attack($attacker,$defender);
                        //$ThisRound->setDamage($_SESSION["damage"]);
                        static::RapidStrike($attacker,$defender);
                        $ThisRound->setDamage($ThisRound->getDamage() + $_SESSION["damage"]);
                        $ThisRound->save();
                    }
                    if ($hero->getLuck() < $beast->getLuck()) { $attacker = $beast; $defender = $hero;
                        $ThisRound->setAttackerId($attacker->getId());
                        $ThisRound->setDefenderId($defender->getId());
                        echo "<br>Loveste prima data $beastName, fiind mai norocos !";
                        static::attack($attacker,$defender);
                        //$ThisRound->setDamage($_SESSION["damage"]);
                        static::RapidStrike($attacker,$defender);
                        $ThisRound->setDamage($ThisRound->getDamage() + $_SESSION["damage"]);
                        $ThisRound->save();
                    }
                }

                if ($hero->getSpeed() > $beast->getSpeed()) { $attacker = $hero; $defender = $beast;
                    $ThisRound->setAttackerId($attacker->getId());
                    $ThisRound->setDefenderId($defender->getId());
                    echo "<br>Avand o viteza mai mare, loveste prima data $heroName !";
                    static::attack($attacker,$defender);
                    //$ThisRound->setDamage($_SESSION["damage"]);
                    static::RapidStrike($attacker,$defender);
                    $ThisRound->setDamage($ThisRound->getDamage() + $_SESSION["damage"]);
                    $ThisRound->save();
                }
                if ($hero->getSpeed() < $beast->getSpeed()) { $attacker = $beast; $defender = $hero;
                    $ThisRound->setAttackerId($attacker->getId());
                    $ThisRound->setDefenderId($defender->getId());
                    echo "<br>Avand o viteza mai mare, loveste prima data $beastName !";
                    static::attack($attacker,$defender);
                   // $ThisRound->setDamage($_SESSION["damage"]);
                    static::RapidStrike($attacker,$defender);
                    $ThisRound->setDamage($ThisRound->getDamage() + $_SESSION["damage"]);
                    $ThisRound->save();
                }

            } else {


            if (isset($_SESSION["player"]) && ($_SESSION["player"] == "Orderus")) {
                $attacker = $hero; $defender = $beast;
                $ThisRound->setAttackerId($attacker->getId());
                $ThisRound->setDefenderId($defender->getId());
                static::attack($attacker,$defender);
                //$ThisRound->setDamage($_SESSION["damage"]);
                $ThisRound->save();

                if (!is_null(static::checkWinner($attacker,$defender,$rounds)) && (static::checkWinner($attacker,$defender,$rounds) != false ))
                { $Battle->setWinnerId(static::checkWinner($attacker,$defender,$rounds));
                    $Battle->save(); session_destroy();
                 die();
                }

                static::RapidStrike($attacker,$defender);
                $ThisRound->setDamage($ThisRound->getDamage() + $_SESSION["damage"]);
                $ThisRound->save();
                static::checkWinner($attacker,$defender,$rounds);
                if (!is_null(static::checkWinner($attacker,$defender,$rounds)) && (static::checkWinner($attacker,$defender,$rounds) != false ))
                { $Battle->setWinnerId(static::checkWinner($attacker,$defender,$rounds));
                    $Battle->save(); session_destroy();
                   die();
                }

            } else {
                if (isset($_SESSION["player"]) && ($_SESSION["player"] == "The Beast")) {
                    $attacker = $beast; $defender = $hero;
                    $ThisRound->setAttackerId($attacker->getId());
                    $ThisRound->setDefenderId($defender->getId());
                    static::attack($attacker,$defender);
                    $ThisRound->setDamage($_SESSION["damage"]);
                    $ThisRound->save();
                    static::checkWinner($attacker,$defender,$rounds);
                    if (!is_null(static::checkWinner($attacker,$defender,$rounds)) && (static::checkWinner($attacker,$defender,$rounds) != false ))
                    { $Battle->setWinnerId(static::checkWinner($attacker,$defender,$rounds));
                        $Battle->save(); session_destroy();
                        die();
                    }
                    static::RapidStrike($attacker,$defender);
                    static::checkWinner($attacker,$defender,$rounds);
                    if (!is_null(static::checkWinner($attacker,$defender,$rounds)) && (static::checkWinner($attacker,$defender,$rounds) != false ))
                    { $Battle->setWinnerId(static::checkWinner($attacker,$defender,$rounds));
                        $Battle->save(); session_destroy();
                        die();
                    }
                    $ThisRound->save();
                }
            }

            }


        }

    }

    public static function tableName(){
        return 'fight';
    }

}