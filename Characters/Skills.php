<?php


namespace Characters;
use Characters\Persona;


class Skills extends Base
{

    public $name;            // name of the persona

    public $value;           //formula to calculate value of the skill

    public $persona_id;     // what persona has this skill

    public $effect;         //how much effect has %

    public $usage;          //when it is used in battle (ex: attack/defence)


    /**
 * @return mixed
 */
public function getName()
{
    return $this->name;
}/**
 * @param mixed $name
 * @return Skills
 */
public function setName($name)
{
    $this->name = $name;
    return $this;
}/**
 * @return mixed
 */
public function getValue()
{
    return $this->value;
}/**
 * @param mixed $value
 * @return Skills
 */
public function setValue($value)
{
    $this->value = $value;
    return $this;
}/**
 * @return mixed
 */
public function getPersonaId()
{
    return $this->persona_id;
}/**
 * @param mixed $persona_id
 * @return Skills
 */
public function setPersonaId($persona_id)
{
    $this->persona_id = $persona_id;
    return $this;
}/**
 * @return mixed
 */
public function getEffect()
{
    return $this->effect;
}/**
 * @param mixed $effect
 * @return Skills
 */
public function setEffect($effect)
{
    $this->effect = $effect;
    return $this;
}/**
 * @return mixed
 */
public function getUsage()
{
    return $this->usage;
}/**
 * @param mixed $usage
 * @return Skills
 */
public function setUsage($usage)
{
    $this->usage = $usage;
    return $this;
}


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }


    public static function tableName(){
        return 'skills';
    }

}