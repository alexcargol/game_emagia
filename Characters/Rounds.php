<?php


namespace Characters;


 class Rounds extends Base
{


    public $game_id;

    public $round_id;

    public $attacker_id;

    public $defender_id;

    public $damage;

     /**
      * @return mixed
      */
     public function getRoundId()
     {
         return $this->round_id;
     }

     /**
      * @param mixed $round_id
      * @return Rounds
      */
     public function setRoundId($round_id)
     {
         $this->round_id = $round_id;
         return $this;
     }



     /**
      * @return mixed
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param mixed $id
      * @return Rounds
      */
     public function setId($id)
     {
         $this->id = $id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getGameId()
     {
         return $this->game_id;
     }

     /**
      * @param mixed $game_id
      * @return Rounds
      */
     public function setGameId($game_id)
     {
         $this->game_id = $game_id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getAttackerId()
     {
         return $this->attacker_id;
     }

     /**
      * @param mixed $attacker_id
      * @return Rounds
      */
     public function setAttackerId($attacker_id)
     {
         $this->attacker_id = $attacker_id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getDefenderId()
     {
         return $this->defender_id;
     }

     /**
      * @param mixed $defender_id
      * @return Rounds
      */
     public function setDefenderId($defender_id)
     {
         $this->defender_id = $defender_id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getDamage()
     {
         return $this->damage;
     }

     /**
      * @param mixed $damage
      * @return Rounds
      */
     public function setDamage($damage)
     {
         $this->damage = $damage;
         return $this;
     }

     public static function tableName(){
         return 'rounds';
     }




 }