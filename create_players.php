<?php


//Hero is created
$Hero = new \Characters\Persona;
$Hero->setName('Orderus');
$Hero->setHealth(rand(70,100));
$Hero->setStrength(rand(70,80));
$Hero->setDefence(rand(45,55));
$Hero->setSpeed(rand(40,50));
$Hero->setLuck(rand(10,30));
$Hero->setTypeId(1);
$Hero->save();

//Adds skill (Rapid Strike) to player
$Skill1 = new  \Characters\Skills;
$Skill1->setName('Rapid Strike');
$skill_value=(rand(1, 100) < 11);
if ($skill_value==true) {$Skill1->setValue('yes');} else {$Skill1->setValue('no');}

$Skill1->setPersonaId($Hero->getId());
$Skill1->setEffect(100);
$Skill1->setUsage('attack');
$Skill1->save();

//Adds skill (Magic Shield) to player
$Skill2 = new  \Characters\Skills;
$Skill2->setName('Magic Shield');
$skill_value=(rand(1, 100) < 21);
if ($skill_value==true) {$Skill2->setValue('yes');} else {$Skill2->setValue('no');}

$Skill2->setPersonaId($Hero->getId());
$Skill2->setEffect(50);
$Skill2->setUsage('defence');
$Skill2->save();


$Beast = new Characters\Persona;
$Beast->setName('The Beast');
$Beast->setHealth(rand(60,90));
$Beast->setStrength(rand(60,90));
$Beast->setDefence(rand(40,60));
$Beast->setSpeed(rand(40,60));
$Beast->setLuck(rand(25,40));
$Beast->setTypeId(2);
$Beast->save();



?>