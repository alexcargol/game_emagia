<?php

include "vendor/autoload.php";
//include "create_players.php";

$mysql = mysqli_connect('localhost', 'root', '', 'eMagia');

function my_autoloader($class) {
    //Characters\Hero -> Characters/Hero
    $path = str_replace('\\','/', $class);
	include  $path . '.php';
   
}

spl_autoload_register('my_autoloader');


function query ($queryString){
    global $mysql;
    $query = mysqli_query($mysql, $queryString);

    if ($query===false){
        die('Eroare query: '.$queryString."MySQL error: ".mysqli_error($mysql));
    }
    if ($query===true){
        return mysqli_insert_id($mysql);
    }


    return mysqli_fetch_all($query, MYSQLI_ASSOC);
}


?>